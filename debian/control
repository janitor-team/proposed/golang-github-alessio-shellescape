Source: golang-github-alessio-shellescape
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Jack Henschel <jackdev@mailbox.org>,
           Arthur Diniz <arthurbdiniz@gmail.com>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any
Standards-Version: 4.5.1
Homepage: https://github.com/alessio/shellescape
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-alessio-shellescape
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-alessio-shellescape.git
XS-Go-Import-Path: github.com/alessio/shellescape
Testsuite: autopkgtest-pkg-go
Rules-Requires-Root: no

Package: golang-github-alessio-shellescape-dev
Architecture: all
Depends: ${misc:Depends}
Description: Escape arbitrary strings for use as command line arguments
 This package provides the shellescape.Quote() function that returns a
 shell-escaped copy of a string. This functionality could be helpful in
 those cases where it is known that the output of a Go program will be
 appended to/used in the context of shell programs command line arguments.
 .
 This work was inspired by the Python original package shellescape
 (https://pypi.python.org/pypi/shellescape).
